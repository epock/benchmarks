****************
Epock Benchmarks
****************

Epock Benchmarks are a set of control and test-case systems which aims at
providing some standards for benchmarking pocket volume measurement / pore
profile calculation software.

This repository is contains two subdirectories:

- ``control`` contains artificial systems in which the pocket volume is know
- ``test_cases`` contains biological system trajectories
  

Control systems
===============

Epock benchmarks thus far contains a single control system, namely `sphere`.
This system is made of a multitude of pseudo-atoms distributed in space
in order to create a spherical void at in the middle of the set of
pseudo-atoms.

As the equation of the points on a sphere is known and the pseudo-atoms
are very closely located, the surface of the resulting void is expected
to be as close as possible from a perfect sphere with known radius, therefore
known surface.


Test cases
==========

Test cases gather together "real" biological systems containing pockets
or channels.

They are useful to evaluate a software performances on real-life problems and
to compare raw data provided by different programs.
