
*******************************
The Estrogen Receptor Test-Case
*******************************

Introduction
============

This is the directory for the benchmark on Estrogen Receptor (ER).

The trajectory is fully described in `Sinha et al., Chembiochem (2010)`_.

This trajectory has been used for the Epock v1.0 benchmark
(see Epock `Supplementary Material`_).

The raw trajectory has had a few pretreatments including:

- atom filtering to remove all atoms but protein atoms,
- fitting on alpha carbones.


Package content
===============

- README.rst: documentation
- Makefile: a make script to convert the trajectory to PDB files (see `Usage`_)
- prot_only.pdb: the Estrogen Receptor system (protein atoms only)
- prot_only.xtc: the Estrogen Receptor trajectory (protein atoms only)
- grid.pdb: a PDB formatted file containing the coordinates of 
  the grid points to use to calculate a pocket volume


Usage
=====

The trajectory can be used as is.
If needed, one can convert it to PDB files using the command ``make``.



.. _Sinha et al., Chembiochem (2010):
    http://onlinelibrary.wiley.com/doi/10.1002/cbic.201000008/abstract

.. _Supplementary Material:
    http://bioinformatics.oxfordjournals.org/content/suppl/2014/12/12/btu822.DC1/SupplementaryMaterial.pdf