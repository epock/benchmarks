
"""Run Epock on a set of PDB files."""

from __future__ import print_function, division

import argparse
import os
import shutil
import subprocess
import sys


def parse_command_line():
    parser = argparse.ArgumentParser()
    parser.add_argument('pdb_dir',
                        help="directory that contains PDB files to be "
                             "processed")
    parser.add_argument('-c', metavar="CONFIG", required=True,
                        help="Epock configuration file")
    parser.add_argument('-g', metavar="GRID", required=True,
                        help="PDB grid file")
    parser.add_argument('--output-dir', default='Epock_output',
                        help="output directory")
    parser.add_argument('--epock-exec', default='epock',
                        help="path to Epock executable")
    args = parser.parse_args()
    if isrelpath(args.epock_exec):
        args.epock_exec = os.path.abspath(args.epock_exec)
    return args


def isrelpath(path):
    """Return true if a path is a relative path."""
    d = os.path.dirname(path)
    return d.startswith('.')


def run_epock(epock_path, pdb_path, cfg_path, output_path):
    """Single Epock run for a given PDB file name."""
    # Run Epock and exit on failure.
    execute_epock = '%(epock_path)s '\
                    '-s %(pdb_path)s '\
                    '-c %(cfg_path)s '\
                    '-o %(output_path)s '\
                    '--ox' % vars()
    p = subprocess.Popen(execute_epock.split(),
                         stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    out, err = p.communicate()
    if not p.returncode == 0:
        print(out + "\n\nAbort: error occured while running Epock")
        exit(p.returncode)


def main():
    args = parse_command_line()
    workdir = os.getcwd()

    # Convert all paths to absolute paths.
    epock_path = args.epock_exec
    pdb_dir = os.path.abspath(args.pdb_dir)
    cfg_path = os.path.abspath(args.c)
    grid_path = os.path.abspath(args.g)
    output_dir = os.path.abspath(args.output_dir)

    pdblist = os.listdir(pdb_dir)

    os.mkdir(output_dir)

    nfiles = len(pdblist)
    for i, pdb_name in enumerate(pdblist):
        # Create a directory for current pdb and go to this directory.
        pdb_id = os.path.splitext(pdb_name)[0]
        tmpdir = os.path.join(output_dir, pdb_id)
        os.mkdir(tmpdir)
        os.chdir(tmpdir)

        # Copy the grid file to current workdir
        shutil.copy(grid_path, ".")

        # Contruct the input PDB file name and the output file name.
        pdb_path = os.path.join(pdb_dir, pdb_name)
        output_path = os.path.join(pdb_name + ".dat")

        # Run Epock.
        run_epock(epock_path, pdb_path, cfg_path, output_path)

        # Go back to workdir.
        os.chdir(workdir)

        print("\r{0:2d}/{1} done".format(i+1, nfiles), file=sys.stderr, end='')
        sys.stderr.flush()

    print('', file=sys.stderr)


if __name__ == "__main__":
    main()
