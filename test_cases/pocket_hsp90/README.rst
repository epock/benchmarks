
********************************
The Heat Shock Protein Test-Case
********************************

Introduction
============

This is the directory for the benchmark on the Heat Shock Protein 90 (HSP90).

This trajectory has been used for the Epock v1.0 benchmark
(see `Epock paper`_ and `Supplementary Material`_)


A dataset of 86 PDB structures of the HSP90 protein determined in different
conformations and with different ligands has been extracted from the list of
molecules presented in `MDpocket’s original paper`_ (Schmidtke et al. 2011).
The structures were superimposed using the MultiSeq VMD plugin
(see `Roberts et al. 2006`_).

This grid is a maximum encompassing region defined as an 8-Å radius sphere
centred at the centre of mass of residues defined as important in the
literature.


Package content
===============

- README.rst: documentation
- HSP90_structures.tar.gz: an archive containing 86 structures of HSP90 
- Makefile: a make script to extrat PDB files from the archive (see `Usage`_)
- prot_only.pdb: the GLIC Channel system (protein atoms only)
- prot_only.xtc: the GLIC Channel trajectory (protein atoms only)
- grid.pdb: a PDB formatted file containing the coordinates of 
  the grid points to use to calculate a pocket volume
- run_epock_pdb.py: python script used to run Epock on a set of PDB files


Usage
=====

First you need to extract the trajectory using the command ``make``.
PDB files are extracted to the directory named ``HSP90_structures``.
In `Epock paper`_, each structures has been treated independently using the
same input grid, ``grid.pdb`` (see ``run_epock_pdb.py``).



.. _Epock paper:
    http://bioinformatics.oxfordjournals.org/content/31/9/1478.full.pdf

.. _Supplementary Material:
    http://bioinformatics.oxfordjournals.org/content/suppl/2014/12/12/btu822.DC1/SupplementaryMaterial.pdf

.. _MDpocket’s original paper:
    http://bioinformatics.oxfordjournals.org/content/early/2011/10/03/bioinformatics.btr550.abstract

.. _Roberts et al. 2006:
    http://bmcbioinformatics.biomedcentral.com/articles/10.1186/1471-2105-7-382
