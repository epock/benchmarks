
**************************
The GLIC Channel Test-Case
**************************

Introduction
============

This is the directory for the benchmark on the GLIC Channel.

This trajectory has been used for the Epock v1.0 benchmark
(see `Epock paper`_ and `Supplementary Material`_)

The raw trajectory has had a few pretreatments including:

- atom filtering to remove all atoms but protein atoms,
- fitting on alpha carbones of residues marking out the pocket i.e.
  residues 201 202 242 255 258.


Package content
===============

- README.rst: documentation
- Makefile: a make script to convert the trajectory to PDB files (see `Usage`_)
- prot_only.pdb: the GLIC Channel system (protein atoms only)
- prot_only.xtc: the GLIC Channel trajectory (protein atoms only)
- grid.pdb: a PDB formatted file containing the coordinates of 
  the grid points to use to calculate a pocket volume


Usage
=====

The trajectory can be used as is.
If needed, one can convert it to PDB files using the command ``make``.



.. _Epock paper:
    http://bioinformatics.oxfordjournals.org/content/31/9/1478.full.pdf

.. _Supplementary Material:
    http://bioinformatics.oxfordjournals.org/content/suppl/2014/12/12/btu822.DC1/SupplementaryMaterial.pdf