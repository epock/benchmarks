#!/usr/bin/env python

"""Create a trajectory made of PDB files of a growing sphere of known
radius which allows to check if a program calculate the right volume."""


from __future__ import print_function, division

import argparse
import math
import os
import subprocess
import sys

from numpy import arange


__author__ = "Benoist LAURENT"


class Atom(object):
    """Gather atom informations together."""
    def __init__(self, **kwargs):
        self.x = 0.0
        self.y = 0.0
        self.z = 0.0
        self.index = 0
        self.name = "X"
        self.resname = "XXX"
        self.chain = "X"
        self.resid = 1
        self.__dict__.update(kwargs)

    def to_pdb(self):
        """Return a PDB formatted string representing the atom."""
        fmt = '%-6s%5s %4s%c%-4s%c%4s%s   %8.3f%8.3f%8.3f%6.2f%6.2f      '\
              '%-4s%2s'

        indexbuf = '*****'
        residbuf = '****'
        altlocchar = ' '
        insertion = ' '
        chain = ' ' if not self.chain else self.chain
        occ = 1.0
        bf = 0.0
        element = self.name[0]

        if self.index < 100000:
            indexbuf = '{0:5d}'.format(self.index)
        elif self.index < 1048576:
            indexbuf = '{0:05x}'.format(self.index)

        if self.resid < 10000:
            residbuf = '{0:4d}'.format(self.resid)
        elif self.resid < 65536:
            residbuf = '{0:04x}'.format(self.resid)

        namebuf = self.name.center(4)
        if len(self.name) > 2:
            namebuf = '{0:>4s}'.format(self.name)

        return fmt % ('ATOM', indexbuf, namebuf, altlocchar,
                      self.resname, chain[0], residbuf, insertion[0],
                      self.x, self.y, self.z, occ, bf,
                      '', element)

    def dist2(self, other):
        """Return the squared distance between 2 atoms."""
        x = (self.x - other.x) * (self.x - other.x)
        y = (self.y - other.y) * (self.y - other.y)
        z = (self.z - other.z) * (self.z - other.z)
        return x + y + z


def which(exe):
    """Try to locate an executable in the PATH environmeent variable."""
    paths = [p for p in os.environ["PATH"].split(os.pathsep)
             if os.path.exists(os.path.join(p, exe))]
    if not any(paths):
        return ''
    return os.path.join(paths[0], exe)


def volume2radius(v):
    """Return the radius of a sphere of volume v."""
    a = math.pi * 4 / 3
    r = (v / a) ** (1./3.)
    return r


def line_count(fn):
    """Return the number of lines in a file."""
    with open(fn, 'rt') as f:
        return len(f.readlines())


def get_rmax(volmax, offset=1.7):
    """Return the radius correponding to a volume with an offset."""
    return volume2radius(volmax) + offset


def make_config(args, template_args):
    """Create configuration file."""
    template_args = dict(token.split('=') for token in template_args)
    rmax = get_rmax(args.end)

    template_args['box_x'] = rmax * 2.5
    template_args['box_y'] = rmax * 2.5
    template_args['box_z'] = rmax * 2.5

    # Read input template and update its content.
    with open(args.template, 'rt') as f:
        content = f.read()
    try:
        content = content % template_args
    except KeyError as e:
        print("ERROR: template parameter {0} not provided".format(e),
              file=sys.stderr)

    # Write output configuration file.
    with open(args.output, 'wt') as f:
        f.write(content)


def get_frame(radius, box, spacing):
    s = ""                  # output string
    r = radius              # radius
    r2 = r * r              # squared radius
    center = Atom()         # sphere center is located at (0, 0, 0)
    d = spacing             # layer thickness
    serial = 0              # current atom serial number

    xmin, xmax, ymin, ymax, zmin, zmax = box

    for x in arange(xmin, xmax, d):
        for y in arange(ymin, ymax, d):
            for z in arange(zmin, zmax, d):
                a = Atom(x=x, y=y, z=z, name="C", resname="ALA", index=serial)
                if a.dist2(center) <= r2:
                    a.x = xmin
                    a.y = ymin
                    a.z = zmin
                s += a.to_pdb() + "\n"
                serial += 1
    return s


def write_frame(content, filename):
    with open(filename, 'wt') as f:
        f.write(content)


def make_trajectory(args, other_args):
    """Create trajectory."""
    if len(other_args) != 0:
        msg = "unknown argument(s) {0} for command 'traj'".format(other_args)
        print("ERROR: ", msg, file=sys.stderr)
        exit(1)

    if args.output:
        if not which('gmx'):
            raise OSError("cannot find executable 'gmx' needed for conversion "
                          "to xtc format")

    # Space between the trajectory atom.
    spacing = 0.3

    # Radius of a carbon atom.
    offset = 1.7

    # Maximum radius.
    rmax = get_rmax(args.end, offset=offset)

    # The box size: 2 times larger than the largest sphere.
    xmin, xmax = -rmax * 1.5, +rmax * 1.5
    ymin, ymax = -rmax * 1.5, +rmax * 1.5
    zmin, zmax = -rmax * 1.5, +rmax * 1.5
    box = (xmin, xmax, ymin, ymax, zmin, zmax)

    volumes = arange(args.start, args.end + args.step, args.step)
    N = len(volumes)
    filelist = ['{0}{1:03d}.pdb'.format(args.prefix, i) for i in range(N)]
    for i, v in enumerate(volumes):
        print("\rFrame {0:>3d}/{1}".format(i+1, N), file=sys.stderr, end='')
        sys.stderr.flush()
        r = volume2radius(v) + offset
        frame = get_frame(r, box, spacing)
        write_frame(frame, filelist[i])

    print("\nTrajectory has {0} particles.".format(line_count(filelist[0])),
          file=sys.stderr)

    if args.output:
        xtc = args.output
        print("Converting to xtc format", file=sys.stderr)
        to_xtc(filelist, xtc)
        print("Successfully created XTC file {}".format(xtc), file=sys.stderr)


def parse_command_line():
    """Setup command-line parser and parse command-line."""
    parser = argparse.ArgumentParser(description=__doc__)

    # Arguments: start volume, final volume, by volume.
    parser.add_argument('start', type=float,
                        help="first frame volume")
    parser.add_argument('end', type=float,
                        help="last frame volume")
    parser.add_argument('step', type=float, default=10., nargs='?',
                        help="volume delta between frames (default=10)")

    # Sub-commands.
    subparsers = parser.add_subparsers(title="command", help="sub-commands")

    # Parser for the "config" command.
    cfg_parser = subparsers.add_parser('config',
                                       description="Create configuration file")
    cfg_parser.add_argument('-o', '--output', type=str, default='epock.cfg',
                            help="output configuration file name")
    cfg_parser.add_argument('-t', '--template', type=str, required=True,
                            help="input configuration template file name")
    cfg_parser.set_defaults(callback=make_config)

    # Parser for the "tarj" command.
    trj_parser = subparsers.add_parser('traj',
                                       description="Create trajectory files")
    trj_parser.add_argument('-p', '--prefix', default='frame',
                            help="output PDB files prefix (default='frame')")
    trj_parser.add_argument('-o', '--output', type=str,
                            help="use Gromacs trjcat utility to create an xtc "
                                 "trajectory")
    trj_parser.set_defaults(callback=make_trajectory)

    return parser.parse_known_args()


def trjcat(filelist, outputtraj):
    """Concatenate pdb files into a single Gromacs XTC file."""
    pipe = subprocess.PIPE
    cmd = ['gmx', 'trjcat', '-cat', '-o', outputtraj, '-f'] + filelist
    try:
        rc = subprocess.call(cmd, stdout=pipe, stderr=pipe)
    except OSError:
        raise OSError('Cannot find gmx trjcat')
    else:
        if rc != 0:
            raise IOError('Error concatenating frames')


def trjconv(inputtraj, outputtraj):
    """Use gromacs trjconv utility to modify the timestep between a
    trajectory frames."""
    pipe = subprocess.PIPE
    cmd = ['gmx', 'trjconv', '-f', inputtraj, '-o', outputtraj,
           '-timestep', '10']
    try:
        rc = subprocess.call(cmd, stdout=pipe, stderr=pipe)
    except OSError:
        raise OSError('Cannot find gmx trjconv')
    else:
        if rc != 0:
            raise IOError('Error concatenating frames')


def to_xtc(filelist, outputfile):
    """Convert a list of pdb files to a Gromacs XTC trajectory."""
    # First concatenate the frames.
    cattrj = 'cat.xtc'
    trjcat(filelist, cattrj)

    # Then modify the timestep between them so that they are assigned a
    # different time.
    trjconv(cattrj, outputfile)

    # Remove temporary xtc file.
    os.remove(cattrj)


def main():
    args, unknown_args = parse_command_line()
    args.callback(args, unknown_args)


if __name__ == '__main__':
    main()
