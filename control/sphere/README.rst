***********************
The sphere control case
***********************

Introduction
============

This system is made of a multitude of pseudo-atoms distributed in space
in order to create a spherical void at in the middle of the set of
pseudo-atoms.

Pseudo-atoms are placed in a box and removed one by one according to its
distance relatively to the center of the box.
This creates a spherical void in the center of a box of pseudo-atoms.

As the pseudo-atoms are very closely located, the surface of the resulting 
void is expected to be as close as possible from a perfect sphere with known
radius, therefore known volume.


Package content
===============

- Makefile: a make script to simplify each step of running a benchmark
- README.rst: documentation.
- config.template: a template configuration file for Epock that will be
  read by setup.py.
- setup.py: a python script which handles the trajectory creation as well
  as the configuration files creation.


Usage
=====

To create a test trajectory with a spherical hole whose volume growths
from 10 to 1000 Å :sup:`3`, by steps of 10 Å :sup:`3`::

    $ make traj START=10 STOP=1000 STEP=10

To run the volume computation program with adapted parameters, define the 
parameters in the ``make`` script and run the program several times.
Here is an example in which the ``GRID_SPACING`` parameter evolves while
the ``PRECISION`` parameter stays constant::

    $ make run PRECISION=2 GRID_SPACING=0.8
    $ make run PRECISION=2 GRID_SPACING=0.5
    $ make run PRECISION=2 GRID_SPACING=0.3
    $ make run PRECISION=2 GRID_SPACING=0.2
    $ make run PRECISION=2 GRID_SPACING=0.1

Results can be visualized with your favorite plot framework.
